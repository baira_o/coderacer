/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import model.Car;
import model.CarType;
import model.Unit;

import java.awt.*;
import java.lang.StrictMath.*;

import static java.lang.StrictMath.*;
import static java.lang.StrictMath.PI;

/**
 * Created by user on 01.12.2015.
 */


public class BuggyProjectileShooter extends ProjectileShooter {

    private BuggyProjectileShooter() {}

    public static ProjectileShooter getInstance() {
        return shooter;
    }

    @Override
    public  boolean needToThrowProjectile(Car self, Car[] otherCars, double washerSpeed) {
        Point point;

        for (Car car: otherCars) {
            if (car.getPlayerId() != self.getPlayerId()) {
                point = findIntersectionPoint(self, car);
                if (point != null) {
                    Point leftPoint = findIntersectionPoint(self.getAngle() - PI / 180 * 2, self.getX(), self.getY(), car);
                    Point rightPoint = findIntersectionPoint(self.getAngle() + PI / 180 * 2, self.getX(), self.getY(), car);
                    if (MyStrategy.USE_VISUAL_CLIENT) {
                        MyStrategy.visualClient.drawPoint(point, 50, Color.yellow);
                        MyStrategy.visualClient.drawPoint(leftPoint, 30, Color.pink);
                        MyStrategy.visualClient.drawPoint(rightPoint, 30, Color.pink);
                    }
                    int washerTicksToLeftIntersection = findTickToReach(washerSpeed,
                            new Point(self.getX(), self.getY()), leftPoint);
                    int washerTicksToRightIntersection = findTickToReach(washerSpeed,
                            new Point(self.getX(), self.getY()), rightPoint);
                    int carTicksToLeftIntersection = findTickToReach(car, leftPoint);
                    int carTicksToRightIntersection = findTickToReach(car, rightPoint);

                    if (!isPointForwardUnit(car, leftPoint) && isPointForwardUnit(car, rightPoint)
                            && washerTicksToRightIntersection < carTicksToRightIntersection
                            && isPointForwardUnit(self, leftPoint) && isPointForwardUnit(self, rightPoint)){
                        return true;
                    }
                    if (!isPointForwardUnit(car, rightPoint) && isPointForwardUnit(car, leftPoint)
                            && washerTicksToLeftIntersection < carTicksToLeftIntersection
                            && isPointForwardUnit(self, rightPoint) && isPointForwardUnit(self, leftPoint)){
                        return true;
                    }
                    if (((washerTicksToLeftIntersection < carTicksToLeftIntersection
                            && washerTicksToRightIntersection > carTicksToRightIntersection) ||
                            (washerTicksToLeftIntersection > carTicksToLeftIntersection
                                    && washerTicksToRightIntersection < carTicksToRightIntersection))
                            && Math.abs(self.getAngleTo(point.getX(), point.getY())) < Math.PI/2.0
                            && Math.abs(self.getAngleTo(leftPoint.getX(), leftPoint.getY())) < Math.PI/2.0
                            && Math.abs(self.getAngleTo(rightPoint.getX(), rightPoint.getY())) < Math.PI/2.0){
                        if ((Math.abs(car.getAngleTo(point.getX(), point.getY())) < Math.PI/2.0
                                && !DriverHelper.isGoingBack(car))
                                || (Math.abs(car.getAngleTo(point.getX(), point.getY()))> Math.PI/2.0
                                && DriverHelper.isGoingBack(car))){
                            return true;
                        }
                    }
                }
                else {
                    if ( Math.abs(self.getAngleTo(car)) < ConstantsUtils.getInstance().getMaxAngleToStrike()
                            && self.getDistanceTo(car) < ConstantsUtils.getInstance().getMaxDistanceToStrike()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static BuggyProjectileShooter shooter = new BuggyProjectileShooter();
}
