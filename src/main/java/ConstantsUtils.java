import model.CarType;

/**
 * Created by Baira on 01.12.2015.
 */
public class ConstantsUtils {
    private static ConstantsUtils instance;

    public static ConstantsUtils getInstance() {
        return instance;
    }

    public static void setInstance(CarType carType) {
        instance = new ConstantsUtils();
        if (carType == CarType.BUGGY) {
            instance.maxDistanceFromBonusToNextPoint = 0.01D * Cell.TILE_SIZE;
            instance.distanceFromSelfToNextPoint = Cell.TILE_SIZE / 2;
            instance.nitroAngleEpsilon = Math.PI / 7.0D;
            instance.minCellsToCorner90ToUseNitro = 5;
            instance.minCellsToCorner45ToUseNitro = 4;
            instance.maxDistanceToNextCornerForBrake = 1.25 * Cell.TILE_SIZE;
            instance.maxDistanceToNextCornerForBrakeWithNitro = 2.5 * Cell.TILE_SIZE;
            instance.maxCellDifferenceBetweenPaths = 40;
            instance.maxAngleToStrike = Math.PI/36.0;
            instance.maxDistanceToStrike = 5 * Cell.TILE_SIZE;
            instance.maxTicksDeltaForStrike = 3;
            instance.maxAngleBetweenCarsWithSameDirection = Math.PI / 8.0;
            instance.maxDistanceToCarToSpoilOil = 4 * Cell.TILE_SIZE;
            instance.minDistanceToCarSpoilOil = 0.5 * Cell.TILE_SIZE;
            instance.maxDistanceToCornerToSpoilOil = 0.8 * Cell.TILE_SIZE;
            instance.minDistanceToBonus = 0.5 * Cell.TILE_SIZE;
            instance.maxDistanceToBonus = 5 * Cell.TILE_SIZE;
        } else {
            instance.maxDistanceFromBonusToNextPoint = 0.01D * Cell.TILE_SIZE;
            instance.distanceFromSelfToNextPoint = Cell.TILE_SIZE / 2;
            instance.nitroAngleEpsilon = Math.PI / 7.0D;
            instance.minCellsToCorner90ToUseNitro = 5;
            instance.minCellsToCorner45ToUseNitro = 4;
            instance.maxDistanceToNextCornerForBrake = 1.25 * Cell.TILE_SIZE;
            instance.maxDistanceToNextCornerForBrakeWithNitro = 2.5 * Cell.TILE_SIZE;
            instance.maxCellDifferenceBetweenPaths = 20;
            instance.maxAngleToStrike = Math.PI/36.0;
            instance.maxDistanceToStrike = 5 * Cell.TILE_SIZE;
            instance.maxTicksDeltaForStrike = 3;
            instance.maxAngleBetweenCarsWithSameDirection = Math.PI / 8.0;
            instance.maxDistanceToCarToSpoilOil = 4 * Cell.TILE_SIZE;
            instance.minDistanceToCarSpoilOil = 0.5 * Cell.TILE_SIZE;
            instance.maxDistanceToCornerToSpoilOil = 0.8 * Cell.TILE_SIZE;
            instance.minDistanceToBonus = 0.5 * Cell.TILE_SIZE;
            instance.maxDistanceToBonus = 5 * Cell.TILE_SIZE;
        }
    }

    private ConstantsUtils() {
    }

    private double maxDistanceFromBonusToNextPoint;
    private double distanceFromSelfToNextPoint;
    private double nitroAngleEpsilon;
    private int minCellsToCorner90ToUseNitro;
    private int minCellsToCorner45ToUseNitro;
    private double maxDistanceToNextCornerForBrake;
    private double maxDistanceToNextCornerForBrakeWithNitro;
    private int maxCellDifferenceBetweenPaths;
    private double maxAngleToStrike;
    private double maxDistanceToStrike;
    private int maxTicksDeltaForStrike;
    private double maxAngleBetweenCarsWithSameDirection;
    private double maxDistanceToCarToSpoilOil;
    private double minDistanceToCarSpoilOil;
    private double maxDistanceToCornerToSpoilOil;

    private double minDistanceToBonus;
    private double maxDistanceToBonus;

    public double getMaxDistanceFromBonusToNextPoint() {
        return maxDistanceFromBonusToNextPoint;
    }

    public double getDistanceFromSelfToNextPoint() {
        return distanceFromSelfToNextPoint;
    }

    public double getNitroAngleEpsilon() {
        return nitroAngleEpsilon;
    }

    public int getMinCellsToCorner90ToUseNitro() {
        return minCellsToCorner90ToUseNitro;
    }

    public int getMinCellsToCorner45ToUseNitro() {
        return minCellsToCorner45ToUseNitro;
    }

    public double getMaxDistanceToNextCornerForBrake() {
        return maxDistanceToNextCornerForBrake;
    }

    public double getMaxDistanceToNextCornerForBrakeWithNitro() {
        return maxDistanceToNextCornerForBrakeWithNitro;
    }

    public int getMaxCellDifferenceBetweenPaths() {
        return maxCellDifferenceBetweenPaths;
    }

    public double getMaxAngleToStrike() {
        return maxAngleToStrike;
    }

    public double getMaxDistanceToStrike() {
        return maxDistanceToStrike;
    }

    public int getMaxTicksDeltaForStrike() {
        return maxTicksDeltaForStrike;
    }


    public double getMinDistanceToBonus() {
        return minDistanceToBonus;
    }

    public double getMaxDistanceToBonus() {
        return maxDistanceToBonus;
    }

    public double getMaxAngleBetweenCarsWithSameDirection() {
        return maxAngleBetweenCarsWithSameDirection;
    }

    public double getMaxDistanceToCarToSpoilOil() {
        return maxDistanceToCarToSpoilOil;
    }

    public double getMinDistanceToCarSpoilOil() {
        return minDistanceToCarSpoilOil;
    }

    public double getMaxDistanceToCornerToSpoilOil() {
        return maxDistanceToCornerToSpoilOil;
    }
}
