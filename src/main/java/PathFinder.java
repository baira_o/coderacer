import model.Car;
import model.Direction;
import model.World;

import java.util.*;

/**
 * Created by Baira on 27.11.2015.
 */
public class PathFinder {
    private static class Node {
        private Node parent;
        private Cell cell;
        private int distance;

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public Node getParent() {
            return parent;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public Cell getCell() {
            return cell;
        }

        public Node(Cell cell) {
            this.cell = cell;
            this.distance = 0;
        }
    }

    public static List<Cell> findPath(Field field, Cell startCell, Cell endCell, List<Cell> excludedCell) {
        HashSet<Cell> discoveredCells = new HashSet<>();
        Queue<Node> cellsQueue = new LinkedList<>();
        LinkedList<Cell> path = new LinkedList<>();

        Node fromNode = new Node(startCell);
        cellsQueue.add(fromNode);
        discoveredCells.add(startCell);

        while(!cellsQueue.isEmpty()) {
            Node vertex = cellsQueue.poll();

            if (vertex.getCell().equals(endCell)) {
                Node parent = vertex.getParent();
                path.addFirst(endCell);

                while (parent != null ) {
                    path.addFirst(parent.getCell());
                    parent = parent.getParent();
                }
                return path;
            }
            List<Cell> neighbors = field.getNeighbors(vertex.getCell());
            for (Cell neighbor : neighbors) {
                if (!discoveredCells.contains(neighbor) && (null == excludedCell
                        || (!excludedCell.contains(neighbor) || vertex.distance > 0))) {
                    Node neighborNode = new Node(neighbor);
                    neighborNode.setDistance(vertex.getDistance() + 1);
                    neighborNode.setParent(vertex);

                    discoveredCells.add(neighbor);
                    cellsQueue.add(neighborNode);
                }
            }
        }
        return path;
    }

    public static List<Cell> findPathWithoutReturn(Field field, Cell startCell, Cell endCell, Direction carDirection) {
        final List<Cell> directPath = findPath(field, startCell, endCell, null);
        if (null == carDirection){
            return directPath;
        }
        Cell nextCell = directPath.get(1);
        Direction dirToNextCell = startCell.locatedTo(nextCell);
        if (!Cell.isAgainst(dirToNextCell, carDirection)){
            return directPath;
        }
        ArrayList<Cell> exclusionCells = new ArrayList<>();
        exclusionCells.add(nextCell);
        List<Cell> secondPath = findPath(field, startCell, endCell, exclusionCells);
        if (null != secondPath && secondPath.size() > 0){
            if (directPath.size() + ConstantsUtils.getInstance().getMaxCellDifferenceBetweenPaths() > secondPath.size()){
                return secondPath;
            } else {
                return directPath;
            }
        }
        return directPath;
    }
}
