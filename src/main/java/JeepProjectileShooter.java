/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import model.Car;

import java.awt.*;
import java.util.*;

import static java.lang.StrictMath.PI;
import static java.lang.StrictMath.pow;

/**
 * Created by user on 01.12.2015.
 */
public class JeepProjectileShooter extends ProjectileShooter {

    private JeepProjectileShooter() {}

    public static JeepProjectileShooter getInstance() {
        return shooter;
    }

    @Override
    public boolean needToThrowProjectile(Car self, Car[] otherCars, double washerSpeed) {
        Point point;

        for (Car car: otherCars) {
            if (car.getPlayerId() != self.getPlayerId()) {
                point = findIntersectionPoint(self, car);
                if (point != null) {
                    /*if (MyStrategy.USE_VISUAL_CLIENT) {
                        MyStrategy.visualClient.drawPoint(point, 50, Color.yellow);
                    }*/
                    isFreePath(new Point(self.getX(), self.getY()), point);
                    int carTickToPoint = findTickToReach(car, point);
                    int washerTickToPoint = findTickToReach(washerSpeed,
                            new Point(self.getX(), self.getY()), point);
                    int tickDelta = Math.abs(washerTickToPoint - carTickToPoint);
                    double distDelta = Math.abs(tickDelta * DriverHelper.getCarSpeed(car));
                    double distDeltaTire = Math.abs(tickDelta * washerSpeed);
                    if (DriverHelper.getCarSpeed(car) < 0.1
                            && car.getDistanceTo(point.getX(), point.getY()) < (Cell.PROJECTILE_RADIUS + car.getWidth() / 2)
                            && Math.abs(self.getAngleTo(point.getX(), point.getY())) < Math.PI/2.0
                            && isFreePath(new Point(self.getX(), self.getY()), point)){
                        return true;
                    }
                    if (Math.abs(self.getAngleTo(point.getX(), point.getY())) < Math.PI/2.0
                            && DriverHelper.getCarSpeed(car) > 0.1
                            && distDelta < (Cell.PROJECTILE_RADIUS + car.getWidth() / 2)
                            && isFreePath(new Point(self.getX(), self.getY()), point)){
                        if ((Math.abs(car.getAngleTo(point.getX(), point.getY())) < Math.PI/2.0
                                && !DriverHelper.isGoingBack(car))
                                || (Math.abs(car.getAngleTo(point.getX(), point.getY()))> Math.PI/2.0
                                && DriverHelper.isGoingBack(car))){
                            return true;
                        }
                    }
                }
                else {
                    if ( Math.abs(self.getAngleTo(car)) < ConstantsUtils.getInstance().getMaxAngleToStrike()
                            && self.getDistanceTo(car) < ConstantsUtils.getInstance().getMaxDistanceToStrike()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static JeepProjectileShooter shooter = new JeepProjectileShooter();

    private static boolean isFreePath(Point start, Point end){
        int count = (int) (end.distanceTo(start)/(Cell.PROJECTILE_RADIUS * 0.8));
        Point delta = end.sub(start).mul(1.0/count);
        Point cur = start;
        for (int i = 0; i < count; i++){
            /*if (MyStrategy.USE_VISUAL_CLIENT) {
                MyStrategy.visualClient.drawPoint(cur, 20, Color.CYAN);
            }*/
            if (!Field.getInstance().isPointInField(cur)){
                break;
            }

            final Point tmp = cur;
            java.util.List<Point> wallBorderPoints = cur.getWallBorderPoints();
            if (wallBorderPoints.isEmpty()) continue;

            Point nearestBorder = Collections.min(wallBorderPoints, new Comparator<Point>() {
                @Override
                public int compare(Point o1, Point o2) {
                    if (tmp.distanceTo(o1) > tmp.distanceTo(o2)){
                        return 1;
                    } else {
                        return -1;
                    }

                }
            });
            /*if (nearestBorder != null  && MyStrategy.USE_VISUAL_CLIENT) {
                MyStrategy.visualClient.drawPoint(nearestBorder, 10, Color.red);
            }*/
            if (nearestBorder != null && nearestBorder.distanceTo(cur) < Cell.PROJECTILE_RADIUS){
                return false;
            }
            cur = cur.add(delta);
        }
        return true;
    }
}
