/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * Created by user on 27.11.2015.
 */

import model.*;

import static java.lang.StrictMath.*;

public abstract class ProjectileShooter {
    protected ProjectileShooter() {
    }

    public static void instance(CarType carType) {
        if (shooter == null)
            switch (carType) {
                case BUGGY:
                    shooter = BuggyProjectileShooter.getInstance();
                    break;
                case JEEP:
                    shooter = JeepProjectileShooter.getInstance();
                    break;
            }
    }

    protected static Point findIntersectionPoint(Unit unit1, Unit unit2) {
        double a, b;
        double c, d;

        a = tan(unit1.getAngle());
        c = tan(unit2.getAngle());

        b = unit1.getY() - a * unit1.getX();
        d = unit2.getY() - c * unit2.getX();

        if (a == c) return null;
        double x = (d - b) / (a - c);

        return new Point(x, a * x + b);
    }

    protected static Point findIntersectionPoint(double angle, double x, double y, Unit unit2) {
        double a, b;
        double c, d;

        a = tan(angle);
        c = tan(unit2.getAngle());

        b = y - a * x;
        d = unit2.getY() - c * unit2.getX();

        if (a == c) return null;
        double xInter = (d - b) / (a - c);

        return new Point(xInter, a * xInter + b);
    }

    protected int findTickToReach(Unit unit, Point point) {
        return (int) (unit.getDistanceTo(point.getX(), point.getY())
                / hypot(unit.getSpeedX(), unit.getSpeedY()));
    }

    protected int findTickToReach(double speed, Point begin, Point end) {
        return (int) (hypot(end.getX() - begin.getX(), end.getY() - begin.getY())
                / speed);
    }

    public static boolean isPointForwardUnit(Unit unit, Point point) {
        return Math.abs(unit.getAngleTo(point.getX(), point.getY())) < Math.PI / 2.0;
    }

    public static ProjectileShooter getInstanceShooter() {
        return shooter;
    }

    abstract public boolean needToThrowProjectile(Car self, Car[] otherCars, double washerSpeed);

    private static ProjectileShooter shooter = null;
}

