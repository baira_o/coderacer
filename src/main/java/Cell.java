import model.Direction;
import model.TileType;

/**
 * Created by Baira on 26.11.2015.
 */
public class Cell {
    public static double TILE_SIZE;
    public static double TILE_MARGIN;
    public static double PROJECTILE_RADIUS;

    private int x;
    private int y;
    private TileType tileType;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public TileType getTileType() {
        return tileType;
    }

    public Cell(int x, int y, TileType tileType) {
        this.x = x;
        this.y = y;
        this.tileType = tileType;
    }

    public Point getCenter() {
        return new Point( (x+0.5) * TILE_SIZE, (y+0.5) * TILE_SIZE);
    }

    public boolean hasConnection(Cell other) {
        Direction meLocatedToOtherDirection = locatedTo(other);
        switch (meLocatedToOtherDirection)
        {
            case DOWN:
                return Cell.hasVerticalConnection(other, this);
            case UP:
                return Cell.hasVerticalConnection(this, other);
            case LEFT:
                return Cell.hasHorizontalConnection(other, this);
            case RIGHT:
                return Cell.hasHorizontalConnection(this, other);
        }
        return false;
    }

    public boolean isNeighbour(Cell other) {
        return Math.abs(other.getX() - x) == 1 && other.getY() == y
               || Math.abs(other.getY() - y) == 1 && other.getX() == x;
    }

    public double getDistanceTo(Cell other) {

        return Math.hypot(other.getX() - x, other.getY() - y);
    }

    @Override
    public boolean equals(Object other)
    {
        if ( this == other ) return true;
        if ( !(other instanceof Cell) ) return false;
        Cell that = (Cell)other;

        return (that.getX() == x) && (that.getY() == y);
    }

    @Override
    public String toString() {
        return x + " " + y + " ";
    }

    public Direction locatedTo(Cell other) {
        if (x == other.getX()) {
            if (y + 1 == other.getY())
                return Direction.DOWN;
            if (y - 1 == other.getY())
                return Direction.UP;
        }
        if (y == other.getY()) {
            if (x + 1 == other.getX())
                return Direction.RIGHT;
            if (x - 1 == other.getX())
                return Direction.LEFT;
        }
        return null;
    }

    private static boolean hasVerticalConnection(Cell upperCell, Cell downerCell)
    {
        return upperCell.getTileType() != TileType.EMPTY
                && downerCell.getTileType() != TileType.EMPTY

                && upperCell.getTileType() != TileType.HORIZONTAL
                && downerCell.getTileType() != TileType.HORIZONTAL

                && downerCell.getTileType() != TileType.RIGHT_BOTTOM_CORNER
                && downerCell.getTileType() != TileType.LEFT_BOTTOM_CORNER

                && upperCell.getTileType() != TileType.RIGHT_TOP_CORNER
                && upperCell.getTileType() != TileType.LEFT_TOP_CORNER

                && downerCell.getTileType() != TileType.TOP_HEADED_T
                && upperCell.getTileType() != TileType.BOTTOM_HEADED_T;
    }

    private static boolean hasHorizontalConnection(Cell leftCell, Cell rightCell)
    {
        return leftCell.getTileType() != TileType.EMPTY
                && rightCell.getTileType() != TileType.EMPTY

                && leftCell.getTileType() != TileType.VERTICAL
                && rightCell.getTileType() != TileType.VERTICAL

                && leftCell.getTileType() != TileType.RIGHT_TOP_CORNER
                && leftCell.getTileType() != TileType.RIGHT_BOTTOM_CORNER

                && rightCell.getTileType() != TileType.LEFT_TOP_CORNER
                && rightCell.getTileType() != TileType.LEFT_BOTTOM_CORNER

                && leftCell.getTileType() != TileType.LEFT_HEADED_T
                && rightCell.getTileType() != TileType.RIGHT_HEADED_T;
    }

    public static boolean isAgainst(Direction first, Direction second){
        if (first.equals(Direction.LEFT) && second.equals(Direction.RIGHT)){
            return true;
        }
        if (first.equals(Direction.RIGHT) && second.equals(Direction.LEFT)){
            return true;
        }
        if (first.equals(Direction.UP) && second.equals(Direction.DOWN)){
            return true;
        }
        if (first.equals(Direction.DOWN) && second.equals(Direction.UP)){
            return true;
        }
        return false;
    }

    public boolean isCorner() {
        return tileType == TileType.RIGHT_TOP_CORNER
                || tileType == TileType.LEFT_TOP_CORNER
                || tileType == TileType.RIGHT_BOTTOM_CORNER
                || tileType == TileType.LEFT_BOTTOM_CORNER;
    }
}
