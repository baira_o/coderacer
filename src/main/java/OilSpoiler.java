/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import model.Car;

import static java.lang.StrictMath.*;

/**
 * Created by user on 01.12.2015.
 */
public class OilSpoiler {
    private static boolean isCarBehind(Car self, Car anotherCar) {
        double eps = 7.0*PI/8.0;
        return Math.abs(self.getAngleTo(anotherCar)) > eps;
    }

    private static boolean isCarsHasSameDirection(Car self, Car otherCar) {
        double eps = ConstantsUtils.getInstance().getMaxAngleBetweenCarsWithSameDirection();
        return abs(self.getAngle() - otherCar.getAngle()) < eps;
    }

    public static boolean needToSpillOil(Car self, Car[] cars, double distanceToCorner) {
        double delta = ConstantsUtils.getInstance().getMaxDistanceToCarToSpoilOil();

        Cell carLocation = new Point(self.getX(), self.getY()).getCell();
        for (Car car : cars) {
            if (car.getPlayerId() != self.getPlayerId() && distanceToCorner
                    < ConstantsUtils.getInstance().getMaxDistanceToCornerToSpoilOil()
                    && !DriverHelper.isGoingBack(self)) {
                if (isCarBehind(self, car) && self.getDistanceTo(car) < delta
                        && self.getDistanceTo(car) > ConstantsUtils.getInstance().getMinDistanceToCarSpoilOil()
                        && isCarsHasSameDirection(self, car)) {
                    return true;
                }
            }
        }
        return false;
    }
}
