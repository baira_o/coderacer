import model.TileType;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Baira on 26.11.2015.
 */
public class Point {
    public static double TILE_SIZE;

    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Cell getCell() {
        return Field.getCell(this);
    }

    @Override
    public String toString() {
        return x + " " + y + " ";
    }

    public List<Point> getWallBorderPoints() {
        Point relativeToPoint = this;

        List<Point> points = new LinkedList<>();
        double cellSize = TILE_SIZE;
        Cell cell = getCell();

        double left = cell.getX() * cellSize;
        double right = (cell.getX()+1) * cellSize;
        double bottom =cell.getY() * cellSize;
        double top = (cell.getY()+1) * cellSize;

        TileType tileType = cell.getTileType();
        
        switch (tileType) {
            case VERTICAL:
                points.add(new Point(left + Cell.TILE_MARGIN, relativeToPoint.getY() ));
                points.add(new Point(right -Cell.TILE_MARGIN, relativeToPoint.getY() ));
                break;
            case HORIZONTAL:
                points.add(new Point(relativeToPoint.getX(), bottom + Cell.TILE_MARGIN ));
                points.add(new Point(relativeToPoint.getX(), top - Cell.TILE_MARGIN ));
                break;
            case LEFT_TOP_CORNER:
                points.add(new Point(relativeToPoint.getX(), bottom + Cell.TILE_MARGIN ));
                points.add(new Point(left + Cell.TILE_MARGIN, relativeToPoint.getY() ));

                points.add(new Point(right - Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                break;
            case RIGHT_TOP_CORNER:
                points.add(new Point(relativeToPoint.getX(), bottom + Cell.TILE_MARGIN ));
                points.add(new Point(right -Cell.TILE_MARGIN, relativeToPoint.getY() ));

                points.add(new Point(left + Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                break;
            case LEFT_BOTTOM_CORNER:
                points.add(new Point(relativeToPoint.getX(), top - Cell.TILE_MARGIN ));
                points.add(new Point(left + Cell.TILE_MARGIN, relativeToPoint.getY() ));

                points.add(new Point(right - Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                break;
            case RIGHT_BOTTOM_CORNER:
                points.add(new Point(relativeToPoint.getX(), top - Cell.TILE_MARGIN ));
                points.add(new Point(right -Cell.TILE_MARGIN, relativeToPoint.getY() ));

                points.add(new Point(left + Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                break;
            case LEFT_HEADED_T:
                points.add(new Point(right -Cell.TILE_MARGIN, relativeToPoint.getY() ));

                points.add(new Point(left + Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                points.add(new Point(left + Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                break;
            case RIGHT_HEADED_T:
                points.add(new Point(left + Cell.TILE_MARGIN, relativeToPoint.getY() ));

                points.add(new Point(right - Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                points.add(new Point(right - Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                break;
            case TOP_HEADED_T:
                points.add(new Point(left + Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                points.add(new Point(right - Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));

                points.add(new Point(relativeToPoint.getX(), top - Cell.TILE_MARGIN ));
                break;
            case BOTTOM_HEADED_T:
                points.add(new Point(left + Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                points.add(new Point(right - Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));

                points.add(new Point(relativeToPoint.getX(), bottom + Cell.TILE_MARGIN ));
                break;
            case CROSSROADS:
                points.add(new Point(right - Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                points.add(new Point(right - Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                points.add(new Point(left + Cell.TILE_MARGIN/2, bottom + Cell.TILE_MARGIN/2 ));
                points.add(new Point(left + Cell.TILE_MARGIN/2, top - Cell.TILE_MARGIN/2 ));
                break;
            case EMPTY:
            case UNKNOWN:
            default:
        }

        return points;
    }

    public Point add(Point other){
        return new Point(x + other.x, y + other.y);
    }

    public Point sub(Point other){
        return new Point(x - other.x, y - other.y);
    }

    public Point mul(double a){
        return new Point(x * a, y * a);
    }

    public double distanceTo(Point other){
        return Math.hypot(other.x - x, other.y - y);
    }
}
