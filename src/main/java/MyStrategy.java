import model.*;

import java.awt.*;
import java.util.ArrayList;

public final class MyStrategy implements Strategy {
    public static VisualClient visualClient = new VisualClient();
    public static final boolean USE_VISUAL_CLIENT = false;

    @Override
    public void move(Car self, World world, Game game, Move move) {

        ProjectileShooter.instance(self.getType());
        ProjectileShooter shooter = ProjectileShooter.getInstanceShooter();

        if (world.getTick() == 0) {
            Point.TILE_SIZE = game.getTrackTileSize();
            Cell.TILE_SIZE = game.getTrackTileSize();
            Cell.TILE_MARGIN = game.getTrackTileMargin();
            Cell.PROJECTILE_RADIUS = game.getTireRadius();
            Field.setInstance(world);
            ConstantsUtils.setInstance(self.getType());
        }
        if (USE_VISUAL_CLIENT) {
            visualClient.beginPre();
            visualClient.drawPoint(Field.getInstance().getCell(self.getNextWaypointX(), self.getNextWaypointY())
                    .getCenter(), 100, Color.GRAY);
        }
        Cell nextWayPoint = Field.getInstance().getCell(self.getNextWaypointX(), self.getNextWaypointY());
        ArrayList<Point> list = MapUtils.getPointsFullPath(self.getNextWaypointIndex(),
        world, Field.getPoint(self).getCell(), DriverHelper.getCarDirection(self));
        //System.out.println(DriverHelper.getCarDirection(self));
        MapUtils.correctPath(list);

        Point next = null;
        Point realNext = null;
        if (list != null && 1 < list.size()) {
            realNext = list.get(1);
            next = list.get(1);
            if (list.size() > 1){
                Bonus nearestBonus = MapUtils.getNearestBonusToPoint(next, world.getBonuses());
                int distanceToNext = (int) (ConstantsUtils.getInstance().getDistanceFromSelfToNextPoint());
                if ((null != nearestBonus
                        && ConstantsUtils.getInstance().getMaxDistanceFromBonusToNextPoint()
                        > Field.getPoint(nearestBonus).distanceTo(next))){
                    distanceToNext = 0;//(int) (Cell.TILE_SIZE / 8);
                }
                if (distanceToNext > next.distanceTo(Field.getPoint(self))){
                    next = list.get(2);
                }
            }
        }
        if (next == null) {
            next = nextWayPoint.getCenter();
        }

        if (USE_VISUAL_CLIENT) {
            if (list != null) {
                for (int i = 0; i < list.size() - 1; i++) {
                    visualClient.circle(list.get(i).getX(), list.get(i).getY(), 30, Color.BLUE);
                    visualClient.line(list.get(i).getX(), list.get(i).getY(), list.get(i + 1).getX(), list.get(i + 1).getY(), Color.BLUE);
                }
            }
        }
        Point blockingPoint = DriverHelper.getBlockingWallPoint(self);
        if (blockingPoint != null) {
            moveBack(next, self, move);
            return;
        } 

        /*Car blockingCar = DriverHelper.getBlockingCar(self, world);
        if (blockingCar != null) {
            moveBack(next, self, move);
            return;
        }*/
        if (USE_VISUAL_CLIENT) {
            visualClient.drawPoint(next, 50, Color.red);
        }

        Point nextCornerPoint = MapUtils.getNextCornerPoint(list);
        double distanceToNextCorner = nextCornerPoint != null
                ? nextCornerPoint.distanceTo(Field.getPoint(self))
                : Integer.MAX_VALUE;

        CornerType cornerType = MapUtils.getNextCornerAngle(list);

        int pointsToCheck = (cornerType == CornerType.CORNER90 || cornerType == CornerType.CORNER180)
                ? ConstantsUtils.getInstance().getMinCellsToCorner90ToUseNitro()
                : ConstantsUtils.getInstance().getMinCellsToCorner45ToUseNitro();

        if (MapUtils.isPathDirect(Field.getPoint(self), list, pointsToCheck)
                && Math.abs(self.getAngleTo(next.getX(), next.getY())) < ConstantsUtils.getInstance().getNitroAngleEpsilon()) {
            if (DriverHelper.canUseNitro(self)) {
               // move.setUseNitro(true);
            }
        }

        if (USE_VISUAL_CLIENT) {
            visualClient.drawPoint(next, 50, Color.red);
        }

        Bonus nearestBonus = DriverHelper.getNearestBonus(self, world.getBonuses(), nextCornerPoint, realNext);
        if (nearestBonus != null) {
            Cell nearestBonusCell = Field.getPoint(nearestBonus).getCell();
            boolean isBonusInPath = false;
            int curIndex = 0;
            Point curPoint;
            do {
                if (curIndex >= list.size()) break;
                curPoint = list.get(curIndex);
                isBonusInPath = isBonusInPath || curPoint.getCell().equals(nearestBonusCell);
                ++curIndex;
            } while (!nextCornerPoint.equals(curPoint));

            if (isBonusInPath) {
                next = Field.getPoint(nearestBonus);
                if (USE_VISUAL_CLIENT) {
                    MyStrategy.visualClient.drawPoint(next, 100, Color.magenta);
                }
            }
        }

        double angleToPoint = self.getAngleTo(next.getX(), next.getY());
        double wheelTurn = angleToPoint * 32.0D / Math.PI;

        double speedModule = Math.hypot(self.getSpeedX(), self.getSpeedY());

        boolean brake = (speedModule * speedModule * Math.abs(angleToPoint) > cornerType.getBrakeCoefficient());
        double maxDistanceToNextCornerForBrake = self.getRemainingNitroTicks() > 0
                ? ConstantsUtils.getInstance().getMaxDistanceToNextCornerForBrakeWithNitro()
                : ConstantsUtils.getInstance().getMaxDistanceToNextCornerForBrake();

        if (distanceToNextCorner <= maxDistanceToNextCornerForBrake
                && speedModule > cornerType.getMaxSpeed()) {
            brake = true; 
        }
        int wheelTurnSign = DriverHelper.isGoingBack(self) ? -1 : 1;

        move.setBrake(brake);
        move.setWheelTurn(wheelTurnSign * wheelTurn);
        move.setEnginePower(1.0);

        move.setSpillOil(OilSpoiler.needToSpillOil(self, world.getCars(), distanceToNextCorner));
        move.setThrowProjectile(shooter.needToThrowProjectile(self, world.getCars(),
                self.getType() == CarType.BUGGY ? game.getWasherInitialSpeed() : game.getTireInitialSpeed()));

        if (USE_VISUAL_CLIENT) {
            for (Car car : world.getCars()) {
                visualClient.drawUnitVector(car);
            }
        }
        if (USE_VISUAL_CLIENT) {
            visualClient.endPre();
        }
    }

    private void moveBack(Point pointToMove, Car self, Move move) {
        double angleToPoint = self.getAngleTo(pointToMove.getX(), pointToMove.getY());
        double wheelTurn = angleToPoint * 32.0D / Math.PI;

        move.setWheelTurn(-wheelTurn);
        move.setEnginePower(-1.0);
    }
}
