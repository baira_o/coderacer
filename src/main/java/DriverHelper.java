import model.*;

import java.awt.*;
import java.util.List;

/**
 * Created by Baira on 27.11.2015.
 */
public class DriverHelper {
    public static boolean canUseNitro(Car car) {
        return car.getNitroChargeCount() > 0
                && car.getRemainingNitroCooldownTicks() == 0
                && car.getRemainingNitroTicks() == 0;
    }

    public static boolean canUseOil(Car car) {
        return car.getOilCanisterCount() > 0
                && car.getRemainingOilCooldownTicks() == 0
                && car.getRemainingOiledTicks() == 0;
    }

    public static Point getBlockingWallPoint(Car car) {
        Point carPoint = Field.getPoint(car);
        double distanceEpsilon = 1.02 * car.getWidth();
        double sectorAngle = Math.PI/3.5;

        List<Point> borders = carPoint.getWallBorderPoints();
        for (Point border : borders) {
            if (car.getDistanceTo(border.getX(), border.getY()) < distanceEpsilon
                    && isPointInSector(car, border, sectorAngle)) {
                return border;
            }
        }
        return null;// returns null if there is no blocking wall point
    }

    @Deprecated
    public static Car getBlockingCar(Car self, World world) {
        double minDistance = world.getWidth() * Point.TILE_SIZE;
        double distanceEpsilon = 1.02 * self.getWidth();
        double sectorAngle = Math.PI/5;
        Car nearestCar = null;

        for (Car car : world.getCars()) {
            if (car.getId() != self.getId()) {
                Point carPoint = Field.getPoint(car);
                double dist = self.getDistanceTo(car);

                if (dist < distanceEpsilon && dist < minDistance
                        && isPointInSector(self, carPoint, sectorAngle)
                        && getCarSpeed(car) < 1.5D) {
                    minDistance = dist;
                    nearestCar = car;
                }
            }
        }
        return nearestCar;
    }

    public static Bonus getNearestBonus(Car self, Bonus[] bonuses, Point nearestCornerPoint, Point next) {
        final double distanceEpsilon = 1.1 * Cell.TILE_SIZE;
        final double nextAndBonusMaxDistance = 0.2 * Cell.TILE_SIZE;
        final double angleEpsilon = Math.PI / 36;

        if (MyStrategy.USE_VISUAL_CLIENT) {
            MyStrategy.visualClient.drawPoint(nearestCornerPoint, 100, Color.lightGray);
            MyStrategy.visualClient.drawPoint(next, 100, Color.pink);
        }

        Bonus nearestBonus = null;
        for (Bonus b : bonuses) {
            Point bonusPoint = Field.getPoint(b);

            double distSelfAndBonus = self.getDistanceTo(bonusPoint.getX(), bonusPoint.getY());

            if (distSelfAndBonus < ConstantsUtils.getInstance().getMaxDistanceToBonus()
                    && isPointInSector(self, bonusPoint, angleEpsilon)) {
                double distNextAndBonus = next.distanceTo(bonusPoint);
                double distCornerAndBonus = nearestCornerPoint.distanceTo(bonusPoint);
                double distSelfAndCorner = self.getDistanceTo(nearestCornerPoint.getX(), nearestCornerPoint.getY());

                Color nextAndBonusLineColor = distNextAndBonus < nextAndBonusMaxDistance
                        ? Color.red : Color.black;

                Color selfAndBonusLineColor = distSelfAndBonus < ConstantsUtils.getInstance().getMaxDistanceToBonus()
                        ? Color.red : Color.black;

                Color selfAndCornerLineColor = distSelfAndCorner > distanceEpsilon
                        ? Color.green : Color.gray;

                if (MyStrategy.USE_VISUAL_CLIENT) {
                    MyStrategy.visualClient.line(bonusPoint, next, String.format("%.2f", distNextAndBonus), nextAndBonusLineColor);
                    MyStrategy.visualClient.line(bonusPoint, nearestCornerPoint, String.format("%.2f", distCornerAndBonus), Color.blue);
                    MyStrategy.visualClient.line(bonusPoint, Field.getPoint(self), String.format("%.2f", distSelfAndBonus), selfAndBonusLineColor);

                    MyStrategy.visualClient.line(nearestCornerPoint, Field.getPoint(self), String.format("%.2f", distSelfAndCorner), selfAndCornerLineColor);

                }
                if (distCornerAndBonus > 2.3 * Cell.TILE_SIZE) {
                    if (distSelfAndBonus <= ConstantsUtils.getInstance().getMaxDistanceToBonus()
                            && isPointInSector(self, bonusPoint, angleEpsilon)) {
                        nearestBonus = b;
                        break;
                    }
                }
            }
        }

        //MyStrategy.visualClient.endPre();
        return nearestBonus;
    }

    public static boolean arePointsOnLine(Point p1, Point p2, Point p3, double delta) {
        Point pc = MapUtils.getCenterOfPoints(p1, p3);
        double step = 0.01;
        for (double alpha = step; alpha < 1; alpha += step){
            pc = MapUtils.getSubCenterOfPoints(p1, p3, alpha);
            if (pc.distanceTo(p2) < delta){
                return true;
            }
        }

        return false;
    }

    public static boolean isPointInSector(Unit unit, Point pointToCheck, double angle) {
        return Math.abs(unit.getAngleTo(pointToCheck.getX(), pointToCheck.getY())) < angle;
    }

    public static double getCarSpeed(Car car) {
        return Math.hypot(car.getSpeedX(), car.getSpeedY());
    }

    public static boolean isGoingBack(Car self){
        double x = self.getX() + self.getSpeedX() * 1;
        double y = self.getY() + self.getSpeedY() * 1;
        return Math.abs(self.getAngleTo(x, y)) > Math.PI / 2;
    }

    public static Direction getCarDirection(Car self){
        double angle = self.getAngle();
        while (angle < 0){
            angle += 2 * Math.PI;
        }
        while (angle > 2 * Math.PI){
            angle -= 2 * Math.PI;
        }
        if ((0 <= angle && angle <= Math.PI / 4)
                || (Math.PI * 2 * 7 / 8 <= angle && angle <= 2 * Math.PI)){
            return Direction.RIGHT;
        }
        if (Math.PI / 4 <= angle && angle <= Math.PI * 3 / 4){
            return Direction.DOWN;
        }
        if (Math.PI * 3 / 4 <= angle && angle <= Math.PI * 2 * 5 / 8){
            return Direction.LEFT;
        }
        if (Math.PI * 2 * 5 / 8 <= angle && angle <= Math.PI * 2 * 7 / 8){
            return Direction.UP;
        }
        return null;
    }
}
