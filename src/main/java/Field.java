import model.TileType;
import model.Unit;
import model.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Baira on 27.11.2015.
 */

public class Field {

    private Field(World world) {
        cells = new Cell[world.getWidth()][world.getHeight()];
        height = world.getHeight();
        width = world.getWidth();

        for(int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                cells[i][j] = new Cell(i, j, world.getTilesXY()[i][j]);
            }
        }
    }

    public boolean isCellInField(Cell cell) {
        return 0 <= cell.getX() && cell.getX() < width
                && 0 <= cell.getY() && cell.getY() < height;
    }

    public boolean isPointInField(Point point) {
        return 0 <= point.getX() && point.getX() < width * Cell.TILE_SIZE
                && 0 <= point.getY() && point.getY() < height * Cell.TILE_SIZE;
    }

    public boolean isCellInField(int i, int j) {
        return 0 <= i && i < width && 0 <= j && j < height;
    }

    public Cell getCell(int i, int j) {
        return cells[i][j];
    }

    public List<Cell> getNeighbors(Cell cell) {
        if (!isCellInField(cell)) return null;

        int x = cell.getX();
        int y = cell.getY();

        ArrayList<Cell> neighbors  = new ArrayList();

        if(isCellInField(x - 1, y)) {
            Cell other = getCell(x - 1, y);
            if (cell.hasConnection(other))
                neighbors.add(other);
        }

        if(isCellInField(x, y - 1)) {
            Cell other = getCell(x, y - 1);
            if (cell.hasConnection(other))
                neighbors.add(other);
        }

        if(isCellInField(x + 1, y)) {
            Cell other = getCell(x + 1,y);
            if (cell.hasConnection(other))
                neighbors.add(other);
        }

        if(isCellInField(x, y + 1)) {
            Cell other = getCell(x, y + 1);
            if (cell.hasConnection(other))
                neighbors.add(other);
        }

        return neighbors;
    }

    private Cell[][] cells;
    private int height;
    private int width;

    private static Field FIELD;

    public static void setInstance(World world) {
        FIELD = new Field(world);
    }

    public static Field getInstance() {
        return FIELD;
    }

    public static Cell getCell(Point point) {
        if (point.getX() < 0 || point.getY() < 0) return  null;

        int cellX = (int)(point.getX() / Point.TILE_SIZE);
        int cellY = (int)(point.getY() / Point.TILE_SIZE);
        return FIELD.cells[cellX][cellY];
    }

    public static Point getPoint(Unit unit) {
        return new Point(unit.getX(), unit.getY());
    }
}
