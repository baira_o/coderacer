/**
 * Created by Baira on 27.11.2015.
 */
public enum CornerType {
    CORNER45(10.5 * 2.5 * Math.PI, Integer.MAX_VALUE),
    CORNER90(10.5 * 2.5 * Math.PI, 25),
    CORNER180(5.5 * 2.5 * Math.PI, 7),
    NOT_CORNER(10.5 * 2.5 * Math.PI, Integer.MAX_VALUE);

    public double getBrakeCoefficient() {
        return brakeCoefficient;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    private double brakeCoefficient;
    private double maxSpeed;

    CornerType(double brakeCoefficient, double maxSpeed) {
        this.brakeCoefficient = brakeCoefficient;
        this.maxSpeed = maxSpeed;
    }
}
