import model.Bonus;
import model.Direction;
import model.World;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Pavel on 23.11.2015.
 */
public class MapUtils {
    public static List<Cell> getAvailableNeighbours(Cell p, World world){
        ArrayList<Cell> tilesArray = new ArrayList<Cell>();
        //TileType[][] tiles = world.getTilesXY();
        Cell pUp = new Cell(p.getX(), p.getY() - 1, world.getTilesXY()[p.getX()][p.getY() - 1]);
        Cell pDown = new Cell(p.getX(), p.getY() + 1, world.getTilesXY()[p.getX()][p.getY() + 1]);
        Cell pRight = new Cell(p.getX() + 1, p.getY(), world.getTilesXY()[p.getX() + 1][p.getY()]);
        Cell pLeft = new Cell(p.getX() - 1, p.getY(), world.getTilesXY()[p.getX() - 1][p.getY()]);
        Cell[] neighbours = new Cell[]{pUp, pDown, pLeft, pRight};

        for (Cell tp : neighbours){
            if (Field.getInstance().isCellInField(tp) && p.isNeighbour(tp)){
                tilesArray.add(tp);
            }
        }
        return tilesArray;
    }

    public static ArrayList<Cell> getFullPath(int num, World world, Cell initPoint, Direction initDir){
        int[][] waypoints2 = world.getWaypoints();
        int[][] waypoints = new int[waypoints2.length  * 2][2];
        for (int i = 0; i < waypoints.length; i++){
            waypoints[i][0] = waypoints2[i % waypoints2.length][0];
            waypoints[i][1] = waypoints2[i % waypoints2.length][1];
        }
        Cell prefWayPoint = initPoint;
        Cell curWayPoint;
        ArrayList<Cell> full = new ArrayList<Cell>();
        full.add(initPoint);
        Direction pathDirection = initDir;
        for (int i = num; i < waypoints.length; i++){
            curWayPoint = new Cell(waypoints[i][0], waypoints[i][1], world.getTilesXY()[waypoints[i][0]][waypoints[i][1]]);
            List<Cell> path = PathFinder.findPathWithoutReturn(Field.getInstance(), prefWayPoint, curWayPoint, pathDirection);
            //getNextTileToWayPoint(curWayPoint, world, prefWayPoint);
            path.remove(0);
            if (path != null && !path.isEmpty()){
                full.addAll(path);
                int size = full.size();
                if (size >= 2) {
                    pathDirection = full.get(size - 2).locatedTo(full.get(size - 1));
                }
            } else {
                break;
            }
            prefWayPoint = curWayPoint;
        }
        return full;
    }

    public static ArrayList<Point> getPointsFullPath(int num, World world, Cell initPoint, Direction carDir){
        ArrayList<Cell> fullPath = getFullPath(num, world, initPoint, carDir);
        boolean isNotAddNext = false;
        ArrayList<Point> pointsFullPath = new ArrayList<Point>();
        for (int i = 0; i < fullPath.size() - 2;){
            Cell p1 = fullPath.get(i);
            Cell p2 = fullPath.get(i + 1);
            Cell p3 = fullPath.get(i + 2);
            if (isDirectWay(p1, p2, p3)){
                if (!isNotAddNext){
                    pointsFullPath.add(p1.getCenter());
                }else{
                    isNotAddNext = false;
                }
                //PointsFullPath.add(p2.getCenter(tileSize));
                i = i + 1;
            } else {
                if (p1.equals(p3) && !p1.equals(p2)){
                    if (!isNotAddNext){
                        pointsFullPath.add(p1.getCenter());
                    }else{
                        isNotAddNext = false;
                    }
                    //PointsFullPath.add(p2.getCenter(tileSize));
                    i = i + 1;
                } else {
                    //Point delta = p2.getCenter().sub(p3.getCenter()).mul(0);
                    if (!isNotAddNext) {
                        pointsFullPath.add(p1.getCenter());
                    }
                    //pointsFullPath.add(p2.getCenter());
                    Point tmp = getSubCenterOfPoints(getSubCenterOfPoints(p1.getCenter(), p3.getCenter(), 0.5),
                            p2.getCenter(), 0.5);
                    pointsFullPath.add(tmp);
                    //Point tmp2 = getSubCenterOfPoints(getCenterOfPoints(p3.getCenter(), p2.getCenter()),
                    //        getCenterOfPoints(p3.getCenter(), p1.getCenter()), 0);
                    //pointsFullPath.add(tmp2);
                    //addDeltaWhenDirect(PointsFullPath, PointsFullPath.size() - 4, delta);*/
                    i = i + 1;
                    isNotAddNext = true;
                }

            }
        }
        return pointsFullPath;
    }

    public static void addDeltaWhenDirect(ArrayList<Point> fullPath, int j, Point delta){
        for (int i = j; i >= 2; ){
            Point p1 = fullPath.get(i);
            Point p2 = fullPath.get(i - 1);
            Point p3 = fullPath.get(i - 2);
            if (isDirectWay(p1.getCell(),
                    p2.getCell(),
                    p3.getCell())){
                fullPath.remove(i);
                fullPath.add(i, p1.add(delta));
                i = i - 1;
            } else {
                return;
            }
        }
        if (j >= 1) {
            Point p1 = fullPath.get(1);
            fullPath.remove(1);
            fullPath.add(1, p1.add(delta));
            Point p2 = fullPath.get(0);
            fullPath.remove(0);
            fullPath.add(0, p2.add(delta));
        }
    }

    public static boolean isDirectWay(Cell p1, Cell p2, Cell p3){
        Cell tmp = Field.getInstance().getCell(   p1.getX() + (p3.getX() - p1.getX()) / 2,
                p1.getY() + (p3.getY() - p1.getY()) / 2);
        return tmp.equals(p2);
    }

    public static Point getCenterOfPoints(Point p1 , Point p2){
        return new Point(p1.getX() + (p2.getX() - p1.getX()) / 2, p1.getY() + (p2.getY() - p1.getY()) / 2);
    }

    public static Point getSubCenterOfPoints(Point p1 , Point p2, double alpha){
        return new Point(p1.getX() + (p2.getX() - p1.getX()) * alpha, p1.getY() + (p2.getY() - p1.getY()) * alpha);
    }

    public static Point getNextCornerPoint(List<Point> path) {
        for (int i = 0; i < path.size() - 4; i++){
            Cell c1 = path.get(i).getCell();
            Cell c2 = path.get(i+1).getCell();
            Cell c3 = path.get(i+2).getCell();
            Cell c4 = path.get(i+3).getCell();

            if (!isDirectWay(c1, c2, c3) ) {
                if (isDirectWay(c2, c3, c4)) {
                    return path.get(i+1);
                    //return CornerType.CORNER90;
                }
                else {
                    if (c1.getDistanceTo(c4) < 1.01) {
                        return path.get(i+1);
                        //return CornerType.CORNER180;
                    }
                    return path.get(i+1);
                    //return CornerType.CORNER45;
                }
            }
        }
        return null;
    }
    public static CornerType getNextCornerAngle(List<Point> path) {
        for (int i = 0; i < path.size() - 4; i++){
            Cell c1 = path.get(i).getCell();
            Cell c2 = path.get(i+1).getCell();
            Cell c3 = path.get(i+2).getCell();
            Cell c4 = path.get(i+3).getCell();

            if (!isDirectWay(c1, c2, c3) ) {
                if (isDirectWay(c2, c3, c4)) {
                    return CornerType.CORNER90;
                }
                else {
                    if (c1.getDistanceTo(c4) < 1.01) {
                        return CornerType.CORNER180;
                    }
                    return CornerType.CORNER45;
                }
            }
        }
        return CornerType.NOT_CORNER;
    }
    public static void correctPathForBonuses(List<Point> path, Bonus[] bonuses) {
        for (int i = 0; i < path.size() - 4; i++){
            Cell c1 = path.get(i).getCell();
            Cell c2 = path.get(i+1).getCell();
            Cell c3 = path.get(i+2).getCell();
            Cell c4 = path.get(i+3).getCell();
            Cell c5 = path.get(i+4).getCell();
            Point p2 = path.get(i + 1);
            Point p3 = path.get(i + 2);
            Point p4 = path.get(i + 3);

            if (isDirectWay(c1, c2, c3)) {
                if (isDirectWay(c2, c3, c4)) {
                    //List<Bonus> bonusesInCellInC2 = getBonusesInCell(bonuses, c2);

                    /*for (Bonus b : bonusesInCellInC2) {
                        if (0.2 * Cell.TILE_SIZE < Field.getPoint(b).distanceTo(p2)) {
                            path.set(i + 1, Field.getPoint(b));
                            break;
                        }
                    }
                    for (Bonus b : bonusesInCellInC3) {
                        if (0.2 * Cell.TILE_SIZE < Field.getPoint(b).distanceTo(p3)) {
                            path.set(i + 2, Field.getPoint(b));
                            break;
                        }
                    }*/
                    if (isDirectWay(c3, c4, c5)) {
                        //TODO
                        List<Bonus> bonusesInCellInC3 = getBonusesInCell(bonuses, c3);
                        for (Bonus b : bonusesInCellInC3) {
                            path.set(i + 2, Field.getPoint(b));
                            break;
                        }
                    }
                }
            }
        }
    }

    public static List<Bonus> getBonusesInCell(Bonus[] bonuses, Cell cell){
        ArrayList<Bonus> result = new ArrayList<>();
        for (Bonus b : bonuses){
            if (Field.getPoint(b).getCell().equals(cell)){
                result.add(b);
            }
        }
        return result;
    }

    public static Bonus getNearestBonusToPoint(Point point, Bonus[] bonuses){
        double minDistance = Integer.MAX_VALUE;
        Bonus bonus = null;
        for (Bonus b : bonuses){
            if (minDistance > Field.getPoint(b).distanceTo(point)){
                minDistance = Field.getPoint(b).distanceTo(point);
                bonus = b;
            }
        }
        return bonus;
    }

    public static void correctPath(List<Point> path) {
        for (int i = 0; i < path.size() - 4; i++){
            Cell c1 = path.get(i).getCell();
            Cell c2 = path.get(i+1).getCell();
            Cell c3 = path.get(i+2).getCell();
            Cell c4 = path.get(i+3).getCell();

            if (!isDirectWay(c1, c2, c3) ) {
                if (isDirectWay(c2, c3, c4)) {
                    return;
                }
                else {
                    if (c1.getDistanceTo(c4) < 1.01) {
                        //return CornerType.CORNER180;

                        Point delta = c1.getCenter().sub(c4.getCenter()).mul(0.25);
                        path.set(i, c1.getCenter().add(delta));
                        return;
                    }
                    return;
                }
            }
        }
    }

    public static boolean isPathDirect(Point self, List<Point> path, int pointsToCheck) {
        boolean result = true;
        int k = 0;

        for (int i = 0; i < path.size() - 3; i++) {
            Point c1 = i == 0 ? self : path.get(i);
            Point c2 = path.get(i + 1);
            Point c3 = path.get(i + 2);

            result = result && arePointsOnLine(c1, c2, c3) ;
            ++k;
            if (k+2 >= pointsToCheck) return result;
        }
        return false;
    }

    public static boolean arePointsOnLine(Point p1, Point p2, Point p3) {
        Point pc = getCenterOfPoints(p1, p3);
        double step = 0.01;
        for (double alpha = step; alpha < 1; alpha += step){
            pc = getSubCenterOfPoints(p1, p3, alpha);
            if (pc.distanceTo(p2) < 0.05 * Point.TILE_SIZE){
                return true;
            }
        }

        return false;
    }
}
